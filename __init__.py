import json
import os
import urequests
import utime

import st3m.wifi
from ctx import Context
from st3m.application import Application, ApplicationContext
from st3m.input import InputState


class App(Application):
    CONFIG_FILE_SD = "/sd/pa2influx.json"

    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)

    def on_enter(self, vm):
        super().on_enter(vm)
        self._pressure = 0.0
        self._pressure_rendered = False
        self._last_sent = 0
        self._config = {
            "https": False,
            "host": "",
            "port": 8086,
            "username": "",
            "password": "",
            "database": "",
            "measurement_name": "flow3r",
            "frequency_s": 10,
        }

        # create config file
        if os.path.exists(self.CONFIG_FILE_SD):
            with open(self.CONFIG_FILE_SD) as f:
                self._config = json.load(f)
        else:
            with open(self.CONFIG_FILE_SD, "w") as f:
                json.dump(self._config, f)

        # form the influxdb v1 url
        if not self._config.get("host") or not self._config.get("database"):
            self._influx_url = None
        else:
            self._influx_url = (
                f"http{'s' if self._config['https'] else ''}://"
                + f"{self._config['host']}:{self._config['port']}/"
                + f"write?u={self._config['username']}&p={self._config['password']}"
                + f"&db={self._config['database']}"
            )

    def draw(self, ctx: Context) -> None:
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.font_size = 23
        ctx.font = ctx.get_font_name(8)

        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

        ctx.rgb(1, 1, 1)

        ctx.move_to(0, -15)
        if self._pressure:
            ctx.text(f"{self._pressure}Pa")
            self._pressure_rendered = True
        else:
            ctx.text("pressure unavailable")

        ctx.move_to(0, 15)
        ctx.font_size = 15
        if self._last_sent:
            ctx.text(
                f"last sent {(utime.ticks_ms() / 1000) - self._last_sent:.1f}s ago"
            )
        elif not self._influx_url:
            ctx.text(f"set up {self.CONFIG_FILE_SD}")
        else:
            ctx.text("yet to send data")

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        self._pressure = ins.imu.pressure

        current_time = utime.ticks_ms() / 1000
        if (
            (self._last_sent < current_time - self._config.get("frequency_s", 10))
            and st3m.wifi.is_connected()
            and self._pressure_rendered
        ):
            influx_data = (
                f"{self._config['measurement_name']} pressure={ins.imu.pressure}"
            )
            urequests.post(
                self._influx_url,
                data=influx_data.encode(),
            )
            self._last_sent = utime.ticks_ms() / 1000


# For running with `mpremote run`:
if __name__ == "__main__":
    import st3m.run

    st3m.run.run_view(App(ApplicationContext()))
